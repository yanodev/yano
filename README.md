# yano

YANO is Yet Another Node for bitcoin, but running in Archlinux ARM.

CAUTION: Under development!

# Installing

`wget https://gitlab.com/yanodev/yano/-/raw/master/build.sh | bash`

This will try to do everything necessary for the installation of metapackages, packages and fixings needed to make your Archlinux ARM a Bitcoin node, like [Raspiblitz](https://github.com/rootzoll/raspiblitz/blob/v1.7/build_sdcard.sh).

# Philosophy

* `Yano` means `simple` in the [Tagalog](https://en.wikipedia.org/wiki/Yano) language;
* It also a nice punk rock band from Philippines (I think that punk is an interesting parallel to anarchist projects like archlinux);
