#!/bin/bash

echo "Welcome to YANO (YetAnotherNOde) installer"

# update ssh connection to gitlab
echo "[`date`] connect to gitlab"
eval $(ssh-agent -s) && ssh-add $HOME/.ssh/gitlab
echo "[`date`] connect to gitlab: DONEi\n\n"
echo ""

# FASTENING DATABASE
echo "[`date`] fastening pacman"
sudo true
pacman -S reflector

echo "[`date`] configuring /etc/xdg/reflector/reflector.conf"
echo "--save /etc/pacman.d/mirrorlist" >> /etc/xdg/reflector/reflector.conf
echo "--age 12" >> /etc/xdg/reflector/reflector.conf
echo "--protocol https" >> /etc/xdg/reflector/reflector.conf
echo "--latest 5" >> /etc/xdg/reflector/reflector.conf

systemd enable reflector.service
systemd enable reflector.timer
systemd start reflector.timer
systemd start reflector.service
echo "**********************************************************"
echo ""

# UPDATE DATABASE
echo "[`date`] updating pacman"
pacman -Syyuu
echo "**********************************************************"
echo ""

# DOWNLOAD BASIC TOOLS
echo "[`date`] download basic tools"
pacman -S base-devel
pacman -S git
pacman -S go
echo "**********************************************************"
echo ""

# DOWNLOAD AND INSTALL YAY
echo "[`date`] download and install YAY"
git clone https://aur.archlinux.org/yay.git
cd $HOME/yay && makepkg -si
rm -rf $HOME/yay
echo "**********************************************************"
echo ""

# DOWNLOAD AND INSTALL EACH FOLDER'S PACKAGES AND METAPACKAGES
mkdir -p .yano
folders="fix-locale utils"
remote="https://gitlab.com/yanodev"
for folder in $folders
do
  echo "[`date`] download and install $folder"
  git clone $remote/$folder.git .yano/$folder
  cd $HOME/.yano/$folder && makepkg -si
  echo "[`date`] download and install $folder: DONE\n\n"
done
