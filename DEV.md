# Development

I'm using [qemu](https://github.com/qemu/qemu) to develop; so the scripts isn't tested on a real hardware.

## Using qemu

To run a Archlinux ARM image on qemu, I suggest to run these [steps](https://gist.github.com/thalamus/561d028ff5b66310fac1224f3d023c12), from the [github user thalamus](https://gist.github.com/thalamus/):

## 0a) The following software on said linux system:
    
* qemu-img
* qemu-system-aarch64
* fdisk
* kpartx
* bsdtar
* wget

In archlinux, ou can do this with:

	$> sudo pacman -S qemu qemu-arch-extra fdisk kpartx bsdtar wget

## 0b) Additionally, if you wish to use QEMU you will need to download this:

	$> wget https://github.com/qemu/qemu/raw/master/pc-bios/edk2-aarch64-code.fd.bz2

## 1) Create a suitable directory, download the Arch tarball and create the raw image:

	$> mkdir ~/alarm
	$> cd ~/alarm
	$> wget http://os.archlinuxarm.org/os/ArchLinuxARM-aarch64-latest.tar.gz
	$> qemu-img create archlinux.img <size>G

Where size can be 4, 8, 16, 32...

## 2) Partition the image with fdisk:

	$ fdisk archlinux.img 

* then `g` (to create a new GPT partition table)
* then `n` (to create a new partition), then enter twice, then +200M and enter
* then `t` (to change the type), then 1 for EFI System Partition
* then `n` and enter three times, then w to write changes and exit

## 3) Become root, create the loop devices and format the partitions of the image.
	
	$> sudo -s
	$# kpartx -av archlinux.img
	$# ls /dev/mapper - you should see two new devices - something like loop6p1 / loop6p2
	$# mkfs.vfat /dev/mapper/loop6p1
	$# mkfs.ext4 /dev/mapper/loop6p2

## 4) Mount the partitions and extract the Arch Linux ARM tarball to them.
	
	$# mkdir root
	$# mount /dev/mapper/loop6p2 root
	$# mkdir root/boot
	$# mount /dev/mapper/loop6p1 root/boot 
	$# bsdtar -xpf ArchLinuxARM-aarch64-latest.tar.gz -C root

## 5a) Edit /etc/fstab and create /boot/startup.nsh files.
	
	$# blkid 
	
You'll need the UUID for both partitions - the vfat partition looks like: UUID="XXXX-XXXX"  and the ext4 one looks like UUID="XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"
	
	$# nano root/etc/fstab

Paste the following (with the correct UUIDs to suit your system of course:

	/dev/disk/by-uuid/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX / ext4 defaults 0 0
	/dev/disk/by-uuid/XXXX-XXXX /boot vfat defaults 0 0
	
Exit the file and then:

	$# nano root/boot/startup.nsh
	
The `startup.nsh` is read by the EFI as a last resort. We need this to be able to do the initial boot. This should look exactly like this (with the correct UUID for the ext4 partition):
	
	Image root=UUID=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX rw initrd=\initramfs-linux.img

## 6) Unmount the partitions and exit from the root shell.
	
	$# umount root/boot
	$# umount root
	$# kpartx -d archlinux.img
	$# sync
	$# exit

## 7) QEMU Only: Convert the image to a qcow2 (much smaller), and create flash partitions with EFI BIOS.

	$ qemu-img convert -O qcow2 archlinux.img archlinux.qcow2

Then move the image to your filesystem:

	$ truncate -s 64M flash0.img (this is for the EFI BIOS)
	$ truncate -s 64M flash1.img (this is to store EFI variables)

Unzip the EFI Bios file:
	
	$ dd if=/path/to/edk2-aarch64-code.fd of=flash0.img conv=notrunc

Put the flash0.img, flash1.img and archlinux.qcow2 into a suitable folder. Use a command similar to this to boot the VM:
 
	qemu-system-aarch64 -L ~/bin/qemu/share/qemu \
	-smp 8 \
	-machine virt,highmem=off \
	-cpu cortex-a72 -m 4096 \
	-drive "if=pflash,media=disk,id=drive0,file=flash0.img,cache=writethrough,format=raw" \
	-drive "if=pflash,media=disk,id=drive1,file=flash1.img,cache=writethrough,format=raw" \
	-drive "if=virtio,media=disk,id=drive2,file=archlinux.qcow2,cache=writethrough,format=qcow2" \
	-nic user,model=virtio-net-pci,hostfwd=tcp::50022-:22 -nographic \
	-device virtio-rng-device -device virtio-balloon-device -device virtio-keyboard-device \
	-device virtio-mouse-device -device virtio-serial-device -device virtio-tablet-device \
	-object cryptodev-backend-builtin,id=cryptodev0 \
	-device virtio-crypto-pci,id=crypto0,cryptodev=cryptodev0
  
QEMU should boot the VM automatically after dropping to the EFI shell.

## 8a) If system enters on rootfs, type:

	$# mount /dev/disk/by-uuid/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX new_root

## 8b) Upon successful first boot, you now need to get pacman up and running, update the system and install efibootmgr:

	$# pacman-key --init
	$# pacman-key --populate archlinuxarm
	$# pacman -Syu
	$# pacman -S efibootmgr
	
We now need to create the EFI boot entry:
	
	$# blkid - you'll need the UUID for either vda2 (if in QEMU) or sda2 (if in Parallels)
	$# efibootmgr --disk /dev/Xda --part 1 --create --label "Arch Linux ARM" --loader /Image --unicode 'root=UUID=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX rw initrd=\initramfs-linux.img' --verbose
	
You should get output which shows a list of the boot entries.If you get an error, it's probably because you're using vda instead of sda or vice versa.

## 9a) Poweroff and boot the system again:

	$# poweroff

Start QEMU VM with the same script on item (7);

## 9b) If system enters on `rootfs`:

	$# mount /dev/disk/by-uuid/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX new_root
	$# exit

This will enter on Archlinux ARM with root user. Type the root password (`root`), and then:

	$# su - alarm

## 10) Post Install notes
	
* Arch Linux ARM is a bit basic out of the box - you will probably want to set a suitable locale,  create a user and install some useful stuff;

* You'll find info on doing all of that on the Arch Wiki - https://wiki.archlinux.org  which, in my opinion, is one of the best distro wikis available.

This was originally written by [thalamus](https://gist.github.com/thalamus/561d028ff5b66310fac1224f3d023c12) with some modifications
